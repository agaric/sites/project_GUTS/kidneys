(function ($, Drupal) {
  Drupal.behaviors.myModuleBehavior = {
    attach: function (context, settings) {
      $(context).find('a.reply-comment-link').once('flat-reply-link').each(function () {
        $(this).on('click', function(e) {
          const author_name = $(this).data('author-name');
          const comment_permalink = $(this).data('permalink');
          const ckeditor_name = $('#comment-form textarea', context).attr('id');
          const ckeditor_instance = ckeditor_name.querySelector( '.ck-editor__editable' );
          const link = '<p><em><a href="' + comment_permalink + '">In reply to ' + author_name + ': </a></em></p><br />';
          ckeditor_instance.setData(link);
        });
      });
    }
  };
})(jQuery, Drupal);
